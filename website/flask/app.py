"""
Welcome to the main control app for the wep page. Here you can add
functions, pass variables, and see how the webpages are linked together.
"""
from flask import Flask, render_template
from pathlib import Path


root = Path(__file__).parent
app = Flask(__name__)

# @app.route('/')
# def homepage():
#     return render_template('database_homepage.html',
#                            tabtitle=tabtitle,
#                            webpage_header=webpage_header)


@app.route('/')
def index():
    return render_template('index.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)