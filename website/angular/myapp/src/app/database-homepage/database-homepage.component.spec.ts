import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatabaseHomepageComponent } from './database-homepage.component';

describe('DatabaseHomepageComponent', () => {
  let component: DatabaseHomepageComponent;
  let fixture: ComponentFixture<DatabaseHomepageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DatabaseHomepageComponent]
    });
    fixture = TestBed.createComponent(DatabaseHomepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
