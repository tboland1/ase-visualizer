import { Component } from '@angular/core';

@Component({
  selector: 'app-database-homepage',
  templateUrl: './database-homepage.component.html',
  styleUrls: ['./database-homepage.component.scss', '../../style.css']
})
export class DatabaseHomepageComponent {
  tabtitle = "C2DB";
  webpage_header = "Computational 2D Materials Database";
}

