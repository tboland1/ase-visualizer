import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailssampleComponent } from './detailssample.component';

describe('DetailssampleComponent', () => {
  let component: DetailssampleComponent;
  let fixture: ComponentFixture<DetailssampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailssampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailssampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
