import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { DatabaseHomepageComponent } from './database-homepage/database-homepage.component';
import { AboutComponent } from './about/about.component';


@NgModule({
  declarations: [AppComponent, DatabaseHomepageComponent, AboutComponent],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
