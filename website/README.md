Flask:
- python based making it easy to integrate and pick up 
- used to develop the backend
- create routes and handlers that handle the search functionality and interact with the SQLite database

API:
- Define API endpoints in your Flask application that correspond to the search functionality. 
- These endpoints can be accessed via specific URLs (e.g., /api/search) and can accept parameters to customize the search query. 
- The endpoints should return the search results as JSON responses.

Angular:
- build the frontend application
  - components
  - services
  - user-interfaces.
- Implement the necessary logic and user-interface elements for searching 
  and displaying the data retrieved from the API

Why use Angular:
- create rich and interactive user interfaces with advanced features such as dynamic content rendering, data binding, client-side routing, and more
- building a complex application that requires a highly interactive user interface, Angular is well-suited for building single-page applications (SPAs). SPAs provide a smoother user experience by reducing page reloads and enabling seamless navigation within the application.
Angular follows a component-based architecture, which promotes modularity and reusability of code.
- separation of backend and front end development

