from api_databases import mongo_db_api
from api_constants import database_constants


def search_2d_substrate(query_this):
    try:

        client = mongo_db_api.get_database_client(database_constants.DATABASE_2DMSD)
        db = client[database_constants.DATABASE_2DMSD]
        col = db[database_constants.COLLECTION_2D_ON_SUBSTRATE]
        results = []

        required_fields = {
            "tags.film_elements": 1,
            "tags.substrate_elements": 1,
            "tags.wf_name": 1,
            "tags.unique_id": 1,
            "tags.film_composition": 1,
            "tags.substrate_composition": 1,
            "tags.surface_plane": 1,
            "tags.max_mismatch": 1,
            "tags.max_area": 1,
            "tags.Interface Config": 1,
            "task_label": 1,
            "compound": 1,
            "Binding_Energy.E_bind": 1,
            "Adsorption_Energy": 1
        }

        data = col.find(query_this, required_fields)
        for el in data:
            temp_data = {
                "film_elements": el["tags"]["film_elements"],
                "task_label": el["task_label"],
                "substrate_elements": el["tags"]["substrate_elements"],
                "compound": el["compound"],
                "wf": el["tags"]["wf_name"],
                "uid": el["tags"]["unique_id"],
                "2D": el["tags"]["film_composition"],
                "Substrate": el["tags"]["substrate_composition"],
                "SurfacePlane": str(el["tags"]["surface_plane"]),
                "Mismatch": float(el["tags"]["max_mismatch"]),
                "BE": el["Binding_Energy"]["E_bind"],
                "AE": el["Adsorption_Energy"]["E_ads"],
                "Max Area": float(el["tags"]["max_area"]),
                "interface_config": el["tags"]["Interface Config"]
            }

            results.append(temp_data)

        client.close()
        return results
    except Exception as err:
        raise err


def get_2D_information(table, request):
    try:
        query = {}
        valid = False
        if "wf_name" in request.args:
            wf_name = request.args.get('wf_name')
            query = {"task_label": wf_name}
            valid = True

        if "compound" in request.args:
            compound = request.args.get("compound")
            query = {"compound": compound}
            valid = True

        if valid:
            client = mongo_db_api.get_database_client(database_constants.DATABASE_2DMSD)
            db = client[database_constants.DATABASE_2DMSD]
            col = db[table]
            for i in col.find(query, {"_id": 0}):
                client.close()
                return i

            client.close()
    except Exception as err:
        raise err


def download_all_materials():
    try:
        client = mongo_db_api.get_database_client(database_constants.DATABASE_2DMSD)
        db = client[database_constants.DATABASE_2DMSD]
        col_2d_on_substrate = db[database_constants.COLLECTION_2D_ON_SUBSTRATE]
        col_2d = db[database_constants.COLLECTION_2D]
        col_substrate = db[database_constants.COLLECTION_SUBSTRATE]
        results = []

        required_columns = {
            "tags.film_composition": 1,
            "tags.substrate_composition": 1,
            "task_label": 1,
            "cif": 1
        }

        data = col_2d_on_substrate.find({}, required_columns)
        for el in data:
            two_d = el["tags"]["film_composition"]
            substrate = el["tags"]["substrate_composition"]

            query_1 = {"compound": two_d}
            query_2 = {"compound": substrate}

            data_two_d = None
            data_substrate = None

            for two_d_data in col_2d.find(query_1, {"cif": 1}):
                data_two_d = two_d_data
                break

            for substrate_data in col_substrate.find(query_2, {"cif": 1}):
                data_substrate = substrate_data
                break

            temp_data = {"filename": el["task_label"], "2D_on_Substrate": el["cif"], "2D": data_two_d["cif"],
                         "Substrate": data_substrate["cif"]}
            results.append(temp_data)

        client.close()
        return results
    except Exception as err:
        raise err
