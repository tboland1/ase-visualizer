from setuptools import setup, find_packages

dependencies = [
    'flask',
    'monty',
    'pytest',
    'sphinx', 'sphinx-rtd-theme',
    'sphinx_autodoc_annotation',
]


setup(
    name='vis',
    version='0.1',
    packages=find_packages(),
    python_requires='>=3.8',
    install_requires=dependencies,
    description='Visualization program that reads an sqlite ase.db '
                'format and creates a webpage.',
)

