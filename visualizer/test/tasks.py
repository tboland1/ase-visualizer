from pathlib import Path
from ase.build import bulk as ase_bulk
from ase.io import read


def bulk(symbol):
    atoms = ase_bulk(symbol)
    path = Path('geom.json')
    atoms.write(path)
    return path


def compute(atoms):
    atoms = read(atoms)
    lat = atoms.cell.get_bravais_lattice()
    return {'lattice': lat.name,
            'vars': lat.vars()}


def mkdb(atoms, info):
    from ase.db import connect
    atoms = read(atoms)
    print(atoms)
    print(info)
    data = {'lattice': info['lattice'],
            **info['vars']}
    with connect('atoms.database') as db:
        db.write(
            atoms=atoms,
            key_value_pairs=dict(uid='hello'),
            data=data)
