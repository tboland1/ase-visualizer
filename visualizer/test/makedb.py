from ase.build import bulk
from pathlib import Path
from ase.build import bulk as ase_bulk
from ase.io import read
from ase.db import connect

def compute(atoms):
    lat = atoms.cell.get_bravais_lattice()
    return {'lattice': lat.name,
            'vars': lat.vars()}

def mkdb(atoms, info):
    print(atoms)
    print(info)
    data = {'lattice': info['lattice'],
            **info['vars']}
    with connect('atoms.database') as db:
        db.write(
            atoms=atoms,
            key_value_pairs=dict(uid='hello'),
            data=data)

if __name__ == '__main__':
    atoms = bulk('Si')
    info = compute(atoms)
    mkdb(atoms, info)
