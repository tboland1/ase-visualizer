import sys
import pandas as pd
from ase import Atoms


def make_dict(db: str = "database.database"):
    '''Read data from database and return a dict with data

    Atomic structures are saved as a dict.'''
    from ase.db import connect

    db = connect("database.database")
    query = "etot,c_11,c_22,c_33,c_23,c_13,c_12,dynamic_stability_level,minhessianeig"
    keys = "formula,etot,c_11,c_22,c_33,c_23,c_13,c_12,dynamic_stability_level,minhessianeig"
    keys_to_save = keys.split(",")
    rows = db.select(query)
    data = {key: [] for key in keys_to_save}

    data['atoms'] = []
    for ir, row in enumerate(rows):
        if ir % 100 == 0:
            print(ir)
        for key in keys_to_save:
            data[key].append(row.get(key))
        atoms = row.toatoms()
        dct = atoms.todict()
        if 'initial_magmoms' in dct:
            dct.pop('initial_magmoms')
        data['atoms'].append(dct)
    return data


if __name__ == '__main__':
    # To make data.json call:
    # python todataframe.py --make

    # In other cases dataframe is just read and printed
    args = sys.argv[1:]

    if '--make' in args:
        print('Constructing dataset (~1min).')
        df = pd.DataFrame(make_dict())
        df.to_json('data.json')

    totalset = pd.read_json('data.json')
    print(totalset)

    trainset = totalset.sample(frac=0.9, random_state=4600)
    testset = totalset.drop(trainset.index)
    print('trainset\n', trainset)
    print('testset\n', testset)

    assert len(trainset) + len(testset) == len(totalset), \
        'Something went wrong in test/train set division'

    if '--make-test-train' in args:
        trainset.to_json('train.json')
        testset.to_json('test.json')

    # Easy to instantiate atoms objects
    for index, row in totalset.iterrows():
        atoms = Atoms(**row['atoms'])

