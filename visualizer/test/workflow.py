import htwutil as htw

@htw.workflow
class Workflow:
    symbol = htw.var()

    @htw.task
    def atoms(self):
        return htw.node('bulk', symbol=self.symbol)

    @htw.task
    def computations(self):
        return htw.node('compute', atoms=self.atoms)

    @htw.task
    def databasestuff(self):
        return htw.node('mkdb', atoms=self.atoms,
                        info=self.computations)


def workflow(rn):
    from ase import Atoms
    for i in range(18, 31):
        rn.with_subdirectory(f'{i:02d}').run_workflow(
            Workflow(symbol=Atoms([i]).symbols[0]))
